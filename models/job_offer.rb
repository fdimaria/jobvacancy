class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title,
                :location, :experience, :description,
                :is_active, :updated_on, :created_on

  validates :title, presence: true

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @experience = init_experience(data[:experience])
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    validate!
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  private

  MIN_EXPERIENCE = 0
  MAX_EXPERIENCE = (2**31) - 1

  def init_experience(experience)
    return MIN_EXPERIENCE if [nil, ''].include? experience

    begin
      experience = Integer(experience)
    rescue ArgumentError
      raise ExperienceInvalidError
    end

    raise ExperienceTooSmallError if experience < MIN_EXPERIENCE
    raise ExperienceTooBigError if experience > MAX_EXPERIENCE

    experience
  end
end
