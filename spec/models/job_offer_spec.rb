require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end
  end

  describe '.experience' do
    it 'should be 0 (zero) when it was not specified on creation' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer.experience).to eq 0
    end

    it 'should be 0 (zero) when "" is specified on creation' do
      job_offer = described_class.new(title: 'a title', experience: '')
      expect(job_offer.experience).to eq 0
    end

    it 'should be 0 (zero) when 0 was specified on creation' do
      job_offer = described_class.new(title: 'a title', experience: 0)
      expect(job_offer.experience).to eq 0
    end

    it 'should be 10 (ten) when 10 was specified on creation' do
      job_offer = described_class.new(title: 'a title', experience: 10)
      expect(job_offer.experience).to eq 10
    end

    it 'should raise ExperienceInvalidError when not an integer is specified on creation' do
      expect do
        described_class.new(title: 'a title', experience: 'not an integer')
      end.to raise_error(
        an_instance_of(ExperienceInvalidError)
      )
    end

    it 'should raise ExperienceTooSmallError when -1 is specified on creation' do
      expect do
        described_class.new(title: 'a title', experience: -1)
      end.to raise_error(
        an_instance_of(ExperienceTooSmallError)
      )
    end

    it 'should raise ExperienceTooBigError when 2147483648 is specified on creation' do
      expect do
        described_class.new(title: 'a title', experience: 2_147_483_648)
      end.to raise_error(
        an_instance_of(ExperienceTooBigError)
      )
    end
  end
end
