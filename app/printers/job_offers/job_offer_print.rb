class JobOfferPrint
  attr_reader :id, :title, :location, :experience,
              :description, :is_active

  def initialize(job_offer)
    @id = job_offer.id
    @title = job_offer.title
    @location = job_offer.location
    @experience = init_experience(job_offer.experience)
    @description = job_offer.description
    @is_active = job_offer.is_active
  end

  private

  def init_experience(experience)
    return experience if experience.positive?

    'Not specified'
  end
end
