class JobOfferErrorPrinter
  def initialize
    @error_messages = {
      ExperienceInvalidError => { experience: ['Should be an integer'] }.freeze,
      ExperienceTooSmallError => { experience: ['Should be greater or equal to zero'] }.freeze,
      ExperienceTooBigError => { experience: ['Should be less or equal to 2147483647'] }.freeze
    }
  end

  def print_all(exception)
    e_class = exception.class
    return exception.model.errors if e_class == ActiveModel::ValidationError

    errors_template = {
      title: [],
      location: [],
      experience: [],
      description: []
    }
    @error_messages[e_class].each do |field, messages|
      errors_template[field] = messages
    end
    errors_template
  end
end
