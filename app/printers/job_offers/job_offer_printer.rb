class JobOfferPrinter
  def self.print_all(job_offers_array)
    prints_array = []
    job_offers_array.each do |job_offer|
      job_offer_print = JobOfferPrint.new(job_offer)
      prints_array.push(job_offer_print)
    end
    prints_array
  end
end
