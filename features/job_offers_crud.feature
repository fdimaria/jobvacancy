Feature: Job Offers CRUD
  In order to get employees
  As a job offerer
  I want to manage my offers

  Background:
  	Given I am logged in as job offerer

  Scenario: Create new offer
    When I create a new offer with "Programmer vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list

  Scenario: Update offer
    Given I have "Programmer vacancy" offer in my offers list
    When I change the title to "Programmer vacancy!!!"
    Then I should see a offer updated confirmation message
    And I should see "Programmer vacancy!!!" in my offers list

  Scenario: Update offer with no title twice
    Given I have "Programmer vacancy" offer in my offers list
    And I am editing the offer
    And I change the title to "" in the editing section
    When I change the title to "" in the editing section
    Then I should see the "Please review the errors" message

  Scenario: Delete offer
    Given I have "Programmer vacancy" offer in my offers list
    When I delete it
    Then I should see a offer deleted confirmation message
    And I should not see "Programmer vacancy!!!" in my offers list

  Scenario: Create new offer where experience is not specified
    When I create a new offer with "" as the experience
    Then I should see a offer created confirmation message
    And I should see "Not specified" in my offers list

  Scenario: Create new offer where experience is 10
    When I create a new offer with "10" as the experience
    Then I should see a offer created confirmation message
    And I should see "10" in my offers list

  Scenario: Create new offer where experience is not an integer
    When I create a new offer with "not an integer" as the experience
    Then I should see the "Please review the errors" message
    And I should see the "Should be an integer" message

  Scenario: Create new offer where experience is a negative integer
    When I create a new offer with "-1" as the experience
    Then I should see the "Please review the errors" message
    And I should see the "Should be greater or equal to zero" message

  Scenario: Create new offer where experience is too big
    When I create a new offer with "2147483648" as the experience
    Then I should see the "Please review the errors" message
    And I should see the "Should be less or equal to 2147483647" message

  Scenario: View offer experience in public offers list
    When I create a new offer with "9" as the experience
    And I activate this offer
    Then I should see the "Offer activated" message
    Then I should see "9" in the public offers list

  Scenario: Update offer with an invalid experience
    Given I have an offer with "10" as experience in my offers list
    When I change the experience to "an invalid experience"
    Then I should see the "Please review the errors" message
    And I should see "10" in some field
