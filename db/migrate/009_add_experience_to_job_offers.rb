Sequel.migration do
  up do
    add_column :job_offers, :experience, Integer, default: 0
  end

  down do
    drop_column :job_offers, :experience
  end
end
